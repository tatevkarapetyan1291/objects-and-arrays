//Number of duplicates

const numberOfDuplicates = (nums) =>
  nums.map((value, index) => {
    const segment = nums.slice(0, index+1);
    return segment.filter(v => v === value).length;
  });
numberOfDuplicates([1, 2, 1, 1, 3]) // [1, 1, 2, 3, 1]
numberOfDuplicates(['a', 'a', 'aa', 'a', 'aa']) // [1, 2, 1, 3, 2]

//Object strength

function countObjectStrength(obj) {
    if(obj === null){
      throw console.error('obj is null')
      return;
    }
    if(typeof obj !== "object" && typeof obj !== 'function'){
      throw console.error('obj is not an obcjet or function type')
      return;
    }
    let counter = 0;
    let names = Object.getOwnPropertyNames(obj);
    for (let i = 0; i < names.length; i++) {
      switch (typeof obj[names[i]]) {
        case "undefined":
          counter += 0;
          break;
        case "boolean":
          counter += 1;
          break;
        case "number":
          counter += 2;
          break;
        case "string":
          counter += 3;
          break;
        case "object":
          counter += 5;
          break;
        case "function":
          counter += 7;
          break;
        case "bigint":
          counter += 9;
          break;
        case "symbol":
          counter += 10;
          break;
        default:
          break;
      }
    }
    return counter;
  }
countObjectStrength(Array) // 31 (2 + 3 + 5 + 7 + 7 + 7)
countObjectStrength(Array.prototype) // 247 (2 + 7 * 32)
countObjectStrength([]) // 2
countObjectStrength({some: 'value'}) // 3